import logging
import os

import tango
from flask import Flask
from flask_healthz import HealthError, Healthz, healthz

import config

app_settings = os.getenv("APP_SETTINGS", "config.DevelopmentConfig")
# default to databaseds device
device_to_ping = os.getenv("DEVICE_SERVER", "sys/database/2")

app = Flask(__name__)
APP_SETTINGS = config.DevelopmentConfig
app.config.from_object(app_settings)
level = logging.DEBUG if app.config["DEBUG"] else logging.ERROR
app.logger.setLevel(level)
app.logger.info("APP_SETTINGS: {0}".format(app_settings))
app.register_blueprint(healthz, url_prefix="/healthz")


def liveness():
    """Liveness check"""
    readiness()


def readiness():
    """Readiness check"""
    try:
        connect_database()
    except Exception as e:
        app.logger.error("Cannot connect to the TANGO_HOST database: {0}".format(
                os.getenv("TANGO_HOST")))
        app.logger.debug("Cannot connect to the TANGO_HOST database - exception: {0}".format(
                str(e)))
        raise HealthError(
            "Cannot connect to the TANGO_HOST database: {0}".format(
                os.getenv("TANGO_HOST")
            )
        )


def connect_database():
    """Test the connection to the Tango DatabaseDS"""
    try:
        # check connection to DatabaseDS
        db = tango.Database()
        db.get_db_host()
        # now ping the device - default DB self test
        dev = tango.DeviceProxy(device_to_ping)
        dev.ping()
    except Exception as e:
        raise Exception("Connection failed with: {0}".format(str(e)))
