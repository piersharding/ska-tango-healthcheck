ARG BUILD_IMAGE="artefact.skao.int/ska-tango-images-tango-dsconfig:1.5.9"
ARG BASE_IMAGE="artefact.skao.int/ska-tango-images-tango-dsconfig:1.5.9"
FROM $BUILD_IMAGE AS buildenv

FROM $BASE_IMAGE

USER root

RUN poetry config virtualenvs.create false

RUN pip install --upgrade pip

WORKDIR /app

COPY --chown=tango:tango . /app

RUN poetry export --format requirements.txt --output poetry-requirements.txt --without-hashes && \
    pip install -r poetry-requirements.txt && \
    rm poetry-requirements.txt

USER tango

ENV PYTHONPATH=/app/src:/usr/local/lib/python3.9/site-packages

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0", "--port=3000"]
