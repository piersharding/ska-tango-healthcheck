PROJECT = ska-tango-healthcheck

CI_PROJECT_DIR ?= .

CI_PROJECT_PATH_SLUG ?= $(PROJECT)
CI_ENVIRONMENT_SLUG ?= $(PROJECT)

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#

# include OCI Images support
include .make/oci.mk

# Include Python support
include .make/python.mk

# include core make support
include .make/base.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak


CI_JOB_ID ?= local##pipeline job id
TANGO_HOST ?= tango-databaseds:10000## TANGO_HOST connection to the Tango DS

# Single image in root of project
OCI_IMAGES = $(PROJECT)

PYTHON_BUILD_TYPE = non_tag_setup

PYTHON_SWITCHES_FOR_FLAKE8=--ignore=F401,W503 --max-line-length=180

requirements: ## Install Dependencies
	poetry install

tagpush:
	docker tag \
	artefact.skao.int/ska-tango-healthcheck:0.0.0 \
	registry.gitlab.com/piersharding/ska-tango-healthcheck/healthcheck:0.0.0
	docker push registry.gitlab.com/piersharding/ska-tango-healthcheck/healthcheck:0.0.0

run: ## run flask in docker
	docker run --rm -ti \
	-p 3000:3000 \
	-e TANGO_HOST="$(TANGO_HOST)" \
	artefact.skao.int/$(PROJECT):$(VERSION)

deploy: ## deploy test service with ingress
	kubectl apply -f ska-tango-healthcheck.yaml

testk8s: ## curl liveness endpoint
	curl http://127.0.0.1:80/healthz/live

test: ## curl liveness endpoint
	curl http://127.0.0.1:3000/healthz/live

.PHONY: requirements run test
